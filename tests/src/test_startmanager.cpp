#include "test_startmanager.h"

test_startmanager::test_startmanager()
{

}

TEST_F(test_startmanager, StartManager)
{
    StartManager startManager(nullptr);
    assert(1==1);
}

TEST_F(test_startmanager, instance)
{
    StartManager::instance();
    assert(1==1);
}

TEST_F(test_startmanager, openFilesInWindow)
{
    StartManager *startManager = StartManager::instance();
    QStringList filePathList;
    filePathList<<".cache/deepin/deepin-editor";
    startManager->openFilesInWindow(filePathList);

    filePathList<<".cache/deepin/";
    startManager->openFilesInWindow(filePathList);

    assert(1==1);
}
//initWindowPosition
TEST_F(test_startmanager, initWindowPosition)
{
    Window*window = new Window();
    StartManager *startManager = StartManager::instance();
    startManager->initWindowPosition(window,true);

    assert(1==1);
}
//getFileTabInfo
TEST_F(test_startmanager, getFileTabInfo)
{
    StartManager *startManager = StartManager::instance();
    startManager->getFileTabInfo(".cache/deepin/deepin-editor");

    assert(1==1);
}

//slotCheckUnsaveTab
TEST_F(test_startmanager, slotCheckUnsaveTab)
{
    StartManager *startManager = StartManager::instance();
    startManager->slotCheckUnsaveTab();

    assert(1==1);
}

//checkPath
TEST_F(test_startmanager, checkPath)
{
    StartManager *startManager = StartManager::instance();
    startManager->checkPath(".cache/deepin/deepin-editor");

    assert(1==1);
}
TEST_F(test_startmanager,ifKlu )
{
    StartManager *startManager = StartManager::instance();
    startManager->ifKlu();

    assert(1==1);
}
//loadTheme
TEST_F(test_startmanager,loadTheme )
{
    StartManager *startManager = StartManager::instance();
    startManager->loadTheme("Dark");

    assert(1==1);
}

// bool isMultiWindow();
TEST_F(test_startmanager,isMultiWindow )
{
    StartManager *startManager = StartManager::instance();
    startManager->isMultiWindow();

    assert(1==1);
}

TEST_F(test_startmanager,isTemFilesEmpty )
{
    StartManager *startManager = StartManager::instance();
    startManager->isTemFilesEmpty();

    assert(1==1);
}

TEST_F(test_startmanager,autoBackupFile)
{
    StartManager *startManager = StartManager::instance();
    startManager->autoBackupFile();

    assert(1==1);
}

TEST_F(test_startmanager,recoverFile)
{
    StartManager *startManager = StartManager::instance();
    Window w;
    startManager->recoverFile(&w);

    assert(1==1);
}

TEST_F(test_startmanager,openFilesInTab)
{
    StartManager *startManager = StartManager::instance();
    startManager->openFilesInTab(QStringList());

    startManager->openFilesInTab(QStringList(".cache/deepin/deepin-editor"));

    startManager->slotCloseWindow();
    startManager->slotCreatNewwindow();



    assert(1==1);
}

TEST_F(test_startmanager,analyzeBookmakeInfo)
{
    StartManager *startManager = StartManager::instance();
    startManager->analyzeBookmakeInfo(QString());

    assert(1==1);
}

